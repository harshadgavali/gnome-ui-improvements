
EXTENSIONDIR=build/src
BUILDIR=build/

pack:
	cp -r metadata.json src/stylesheet.css src/schemas $(EXTENSIONDIR)
	gnome-extensions pack -f \
		-o $(BUILDIR) $(EXTENSIONDIR) \
		--extra-source=lib --extra-source=schemas \
		# --extra-source=ui

update: pack
	gnome-extensions install -f build/*.shell-extension.zip