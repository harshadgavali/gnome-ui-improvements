import { imports } from 'gnome-shell';

const Main = imports.ui.main;
const { WindowPreview } = imports.ui.windowPreview;

export class WindowPreviewExtension implements ISubExtension {
	private _ogGetCaption: () => string;

	constructor() {
		this._ogGetCaption = WindowPreview.prototype._getCaption;
		WindowPreview.prototype._getCaption = () => '';
		Main.layoutManager.uiGroup.add_style_class_name('no-window-caption');
	}
	
	destroy(): void {
		Main.layoutManager.uiGroup.remove_style_class_name('no-window-caption');
		WindowPreview.prototype._getCaption = this._ogGetCaption;
	}
}