import { imports } from 'gnome-shell';

const { ThumbnailsBox } = imports.ui.workspaceThumbnail;

declare const global: import('@gi-types/shell').Global;
declare type ThumbnailsBoxT = imports.ui.workspaceThumbnail.ThumbnailsBox;

function alwaysShowThumbnailsBox(this: ThumbnailsBoxT) {
	const { nWorkspaces } = global.workspace_manager;
	const shouldShow = this._settings.get_boolean('dynamic-workspaces') || (nWorkspaces > 1);
	if (this._shouldShow === shouldShow)
		return;
	this._shouldShow = shouldShow;
	this.notify('should-show');
}

export class WorkspacesThumbnailsExtension implements ISubExtension {
	private _default_ThumbnailsShouldShow: () => void;

	constructor() {
		this._default_ThumbnailsShouldShow = ThumbnailsBox.prototype._updateShouldShow;
	}

	apply(): void {
		ThumbnailsBox.prototype._updateShouldShow = alwaysShowThumbnailsBox;
	}

	destroy(): void {
		ThumbnailsBox.prototype._updateShouldShow = this._default_ThumbnailsShouldShow;
	}
}