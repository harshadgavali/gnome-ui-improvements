import GObject from '@gi-types/gobject';
import St from '@gi-types/st';
import Clutter from '@gi-types/clutter';
import Meta from '@gi-types/meta';
import Shell from '@gi-types/shell';
import Gio from '@gi-types/gio';
import { imports } from 'gnome-shell';

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const SHELL_KEYBINDINGS_SCHEMA = 'org.gnome.shell.keybindings';
const { ControlsState } = imports.ui.overviewControls;
const ApplicationsButtonName = 'panelApplications';

declare type ButtonType = imports.ui.panelMenu.Button & { name: string };

const ApplicationsButton = GObject.registerClass(
	class _ApplicationsButtonClass extends PanelMenu.Button implements ButtonType {
		name: string;
		private _label: St.Label;
		constructor() {
			super(0.0, null, true);
			this.name = ApplicationsButtonName;
			this._label = new St.Label({
				text: _('Applications'),
				y_align: Clutter.ActorAlign.CENTER,
			});

			this.add_actor(this._label);
			this.label_actor = this._label;
		}
	},
);

export class ApplicationButtonExtension implements ISubExtension {
	private _uuid: string;
	private _applicationsButton: typeof ApplicationsButton.prototype;
	private _overviewControls: imports.ui.overviewControls.OverviewControlsManager;
	private _overviewAdjustment: imports.ui.overviewControls.OverviewAdjustment;
	private _showAppsButton: St.Button;
	private _activitiesButton: ButtonType;
	private _applicationsEventId = 0;
	private _activitiesEventId = 0;
	private _overviewAdjustmentValueId = 0;

	constructor(uuid: string) {
		this._uuid = uuid;
		this._overviewControls = Main.overview._overview._controls;
		this._overviewAdjustment = this._overviewControls._stateAdjustment;
		this._showAppsButton = Main.overview.dash.showAppsButton;
		this._activitiesButton = Main.panel.statusArea.activities;
		this._applicationsButton = new ApplicationsButton();
	}

	apply(): void {
		Main.panel.addToStatusArea(this._uuid, this._applicationsButton, 1, 'left');

		this._applicationsEventId = this._applicationsButton.connect(
			'event',
			this._actorEvent.bind(this),
		);
		this._activitiesEventId = this._activitiesButton.connect(
			'event',
			this._actorEvent.bind(this),
		);

		this._overviewAdjustmentValueId = this._overviewAdjustment.connect(
			'notify::value',
			this._styleHandler.bind(this),
		);

		this._styleHandler();
		this._setupToggleAppKeyBinding(true);
	}

	destroy(): void {
		this._setupToggleAppKeyBinding(false);

		this._overviewAdjustment.disconnect(this._overviewAdjustmentValueId);

		this._activitiesButton.disconnect(this._activitiesEventId);
		this._applicationsButton.disconnect(this._applicationsEventId);

		this._applicationsButton.destroy();
	}

	private _styleHandler(): void {
		const { initialState, progress, finalState } = this._overviewAdjustment.getStateTransitionParams();
		if (
			(finalState === ControlsState.APP_GRID && progress >= 0.5) ||
			(initialState === ControlsState.APP_GRID && progress < 0.5)
		) {
			this._setOverviewStyle(this._applicationsButton, true);
		} else {
			this._setOverviewStyle(this._applicationsButton, false);
		}

		if (
			(finalState === ControlsState.WINDOW_PICKER && progress >= 0.5) ||
			(initialState === ControlsState.WINDOW_PICKER && progress < 0.5)
		) {
			this._setOverviewStyle(this._activitiesButton, true);
		} else {
			this._setOverviewStyle(this._activitiesButton, false);
		}
	}

	private _setOverviewStyle(actor: ButtonType, value: boolean) {
		if (value && !actor.has_style_pseudo_class('overview')) {
			actor.add_style_pseudo_class('overview');
		} else if (!value && actor.has_style_pseudo_class('overview')) {
			actor.remove_style_pseudo_class('overview');
		}
	}

	private _actorEvent(actor: ButtonType, event: Clutter.Event): boolean {
		if (
			event.type() == Clutter.EventType.TOUCH_END ||
			event.type() == Clutter.EventType.BUTTON_RELEASE
		) {
			if (Main.overview.shouldToggleByCornerOrButton()) {
				this._toggleOverview(actor);
				return Clutter.EVENT_STOP;
			}
		}
		return Clutter.EVENT_PROPAGATE;
	}

	private _toggleOverview(actor: ButtonType): void {
		if (Main.overview.visible) {
			if (actor.name === ApplicationsButtonName) {
				if (this.showAppsButtonChecked) Main.overview.hide();
				else this.showAppsButtonChecked = true;
			} else {
				if (this.showAppsButtonChecked) this.showAppsButtonChecked = false;
				else Main.overview.hide();
			}
		} else {
			if (actor.name === ApplicationsButtonName) Main.overview.showApps();
			else Main.overview.show();
		}
	}

	private _setupToggleAppKeyBinding(enable: boolean): void {
		Main.wm.removeKeybinding('toggle-application-view');
		let toggleFunc;
		if (enable)
			toggleFunc = this._toggleOverview.bind(this, this._applicationsButton);
		else
			toggleFunc = this._overviewControls._toggleAppsPage.bind(this._overviewControls);

		Main.wm.addKeybinding(
			'toggle-application-view',
			new Gio.Settings({ schema_id: SHELL_KEYBINDINGS_SCHEMA }),
			Meta.KeyBindingFlags.IGNORE_AUTOREPEAT,
			Shell.ActionMode.NORMAL | Shell.ActionMode.OVERVIEW,
			toggleFunc,
		);
	}

	get showAppsButtonChecked(): boolean {
		return this._showAppsButton.checked;
	}

	set showAppsButtonChecked(checked: boolean) {
		this._showAppsButton.checked = checked;
	}
}
