import { imports } from 'gnome-shell';

import GObject from '@gi-types/gobject';
import St from '@gi-types/st';
import Clutter from '@gi-types/clutter';
import Gio from '@gi-types/gio';

const Main = imports.ui.main;
const Layout = imports.ui.layout;
const { ControlsState } = imports.ui.overviewControls;

// const SIDE_CONTROLS_ANIMATION_TIME = imports.ui.overview.ANIMATION_TIME;

const CustomSearchEntryBin = GObject.registerClass(
	class GObjectCustomSearchEntryBin extends St.Bin {
		private _searchEntry: St.Bin;

		constructor(SearchEntry: St.Bin) {
			super();

			this._searchEntry = SearchEntry;
			this.add_child(this._searchEntry);
			this.add_constraint(new Layout.MonitorConstraint({ primary: true }));
			this.set_clip_to_allocation(true);

			this.connect('destroy', this._onDestroy.bind(this));
		}

		vfunc_allocate(box: Clutter.ActorBox) {
			const [width] = box.get_size();
			const [searchHeight] = this._searchEntry.get_preferred_height(width ?? 0);
			const childBox = new Clutter.ActorBox();

			childBox.set_origin(0, 0);
			childBox.set_size(width ?? 0, searchHeight ?? 0);
			this._searchEntry.allocate(childBox);

			childBox.set_origin(box.x1, box.y1);
			this.set_allocation(childBox);
		}

		_onDestroy() {
			this.remove_child(this._searchEntry);
		}
	},
);

export class SearchEntryExtension implements ISubExtension {
	private _searchEntry: St.Entry;
	private _overviewControls: imports.ui.overviewControls.OverviewControlsManager;
	private _overviewAdjustment: imports.ui.overviewControls.OverviewAdjustment;
	private _originalSearchEntryBin: St.Bin;
	private _originalBinPosition: number;
	private _dummySearchEntryBin: St.Bin;
	private _customSearchEntryBin: typeof CustomSearchEntryBin.prototype;
	private _overviewChangingId = 0;
	private _overviewShowingId = 0;
	private _overviewHiddenId = 0;

	constructor() {
		this._searchEntry = Main.overview.searchEntry;
		this._overviewControls = Main.overview._overview._controls;
		this._overviewAdjustment = this._overviewControls._stateAdjustment;

		this._originalSearchEntryBin = this._searchEntry.get_parent() as St.Bin;
		this._originalBinPosition = Math.max(0, this._overviewControls.get_children().indexOf(this._originalSearchEntryBin));

		this._dummySearchEntryBin = new St.Bin({ x_align: Clutter.ActorAlign.CENTER });

		this._overviewControls.layoutManager._searchEntry = this._dummySearchEntryBin;
		this._overviewControls.remove_child(this._originalSearchEntryBin);
		this._overviewControls.insert_child_at_index(this._dummySearchEntryBin, this._originalBinPosition);
		this._customSearchEntryBin = new CustomSearchEntryBin(this._originalSearchEntryBin);
	}

	apply(): void {
		// set custom search entry above panel for better interacting with it.
		Main.layoutManager.uiGroup.add_child(this._customSearchEntryBin);
		Main.layoutManager.uiGroup.set_child_above_sibling(this._customSearchEntryBin, Main.panel.get_parent());

		this._overviewChangingId = this._overviewAdjustment.connect('notify::value', this._onOverviewValueChanged.bind(this));
		this._overviewShowingId = Main.overview.connect('showing', this._onOverviewShowing.bind(this));
		this._overviewHiddenId = Main.overview.connect('hidden', this._onOverviewHidden.bind(this));

		this._fixStartupState();
	}

	private _fixStartupState() {
		const shellSettings = new Gio.Settings({ schema: 'org.gnome.shell' });
		const enabledExtensions = shellSettings.get_value<'as'>('enabled-extensions').recursiveUnpack();

		if (enabledExtensions.includes('no-overview@fthx')) {
			this._overviewAdjustment.value = ControlsState.HIDDEN;
		}

		if (this._overviewAdjustment.value === ControlsState.HIDDEN)
			this._onOverviewHidden();
		else
			this._onOverviewShowing();

		this._onOverviewValueChanged();
	}

	destroy(): void {
		Main.overview.disconnect(this._overviewHiddenId);
		Main.overview.disconnect(this._overviewShowingId);
		this._overviewAdjustment.disconnect(this._overviewChangingId);

		Main.layoutManager.uiGroup.remove_child(this._customSearchEntryBin);
		this._customSearchEntryBin.destroy();

		this._overviewControls.layoutManager._searchEntry = this._originalSearchEntryBin;
		this._overviewControls.remove_child(this._dummySearchEntryBin);
		this._overviewControls.insert_child_at_index(this._originalSearchEntryBin, this._originalBinPosition);
		this._dummySearchEntryBin.destroy();

		this._searchEntry.opacity = 255;
		this._searchEntry.translation_y = 0;
		this._searchEntry.reactive = true;
		this._searchEntry.visible = true;
	}

	private _onOverviewShowing(): void {
		// to have proper padding
		const [searchHeight] = this._originalSearchEntryBin.get_preferred_height(0);
		if (searchHeight) {
			this._dummySearchEntryBin.natural_width = this._dummySearchEntryBin.width;
			this._dummySearchEntryBin.min_height = Math.max(searchHeight - Main.panel.height, 0);
		}

		this._customSearchEntryBin.visible = true;
	}

	private _onOverviewHidden(): void {
		this._customSearchEntryBin.visible = false;
	}

	private _onOverviewValueChanged(): void {
		const { initialState, finalState, progress: origProgress } = this._overviewAdjustment.getStateTransitionParams();
		let progress = origProgress;

		const minState = Math.min(initialState, finalState);
		// const maxState = Math.max(initialState, finalState);

		if (minState !== ControlsState.HIDDEN) {
			this._customSearchEntryBin.translation_y = 0;
			this._searchEntry.opacity = 255;
			this._searchEntry.reactive = true;
		} else {
			if (finalState === ControlsState.HIDDEN) {
				progress = 1 - progress;
			}

			this._searchEntry.translation_y = (1 - progress) * (-2 * this._searchEntry.height);
			this._searchEntry.opacity = Math.round(progress * 255);
			this._searchEntry.reactive = progress === 1;
		}
	}
}