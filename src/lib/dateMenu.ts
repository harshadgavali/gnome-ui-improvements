import Clutter from '@gi-types/clutter';
import { BoxLayout } from '@gi-types/st';
import { imports } from 'gnome-shell';

const Main = imports.ui.main;

declare type PanelButton = imports.ui.panelMenu.Button;

export class DateMenuExtension implements ISubExtension {
	private _dateMenu: PanelButton;

	constructor() {
		this._dateMenu = Main.panel.statusArea.dateMenu;
	}

	apply(): void {
		this._changeBox(this._dateMenu, Main.panel._rightBox, -2);
		Main.messageTray.bannerAlignment = Clutter.ActorAlign.END;
	}

	destroy(): void {
		this._changeBox(this._dateMenu, Main.panel._centerBox);
		Main.messageTray.bannerAlignment = Clutter.ActorAlign.CENTER;
	}

	private _changeBox(indicator: PanelButton, newBox: BoxLayout, offset = 0): void {
		const container = indicator.container;
		// remove from old box, if necessary
		const parent = container.get_parent();
		if (parent)
			parent.remove_actor(container);
		// -1 means at the end, -2 means at second from end..
		if (offset < 0) {
			offset = newBox.get_n_children() + 1 + offset;
		}
		newBox.insert_child_at_index(container, offset);
	}
}
