import * as Gio from '@gi-types/gio';
import { ExtensionMeta, imports } from 'gnome-shell';
import { ApplicationButtonExtension } from './lib/applicationButton';
import { DateMenuExtension } from './lib/dateMenu';
import { SearchEntryExtension } from './lib/searchEntry';
import { WindowPreviewExtension } from './lib/windowPreview';
import { WorkspacesThumbnailsExtension } from './lib/workspaceThumbnails';

const ExtensionUtils = imports.misc.extensionUtils;

// declare const global: import('@gi-types/shell').Global;

class Extension implements IExtension {
	private _uuid: string;
	private _settings?: Gio.Settings;
	private _extensions: ISubExtension[] = [];
	private _settingChangedId = 0;

	constructor(meta: ExtensionMeta) {
		this._uuid = meta.uuid;
	}

	enable(): void {
		this._settings = ExtensionUtils.getSettings();
		this._settingChangedId = this._settings.connect('changed', () => {
			this._disable();
			this._enable();
		});
		this._enable();
	}

	disable(): void {
		this._disable();
		this._settings?.disconnect(this._settingChangedId);
		this._settingChangedId = 0;
		this._settings = undefined;
	}

	private _enable(): void {
		this._extensions = [
			new DateMenuExtension(),
			new SearchEntryExtension(),
			new WorkspacesThumbnailsExtension(),
			new ApplicationButtonExtension(this._uuid),
			new WindowPreviewExtension(),
		];

		this._extensions.forEach((ext) => {
			if (ext.apply) ext.apply();
		});
	}

	private _disable(): void {
		this._extensions.reverse().forEach(extension => extension.destroy());
		this._extensions = [];
	}
}

export function init(meta: ExtensionMeta): IExtension {
	return new Extension(meta);
}